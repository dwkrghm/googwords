# googwords

Extract data from google search results page.


## Usage

### Requirements

- Python 3.7+
- Poetry (https://python-poetry.org/)[https://python-poetry.org/]
- Chrome webdriver (https://selenium.dev/documentation/en/webdriver/driver_requirements/#quick-reference)[https://selenium.dev/documentation/en/webdriver/driver_requirements/#quick-reference]

### Installation

    $ git clone https://gitlab.com/dwkrghm/googwords.git
    $ cd googwords
    $ poetry install
    $ cp _env .env

    Generate Secret key and update .env file
    $ poetry run python manage.py generate_secret
    
    Create database and update .env file

    Run migrations
    $ poetry run python manage.py db migrate

    Create superuser with password
    $ poetry run python manage.py db createsuperuser admin myadminpassword
    $ poetry run python manage.py run
