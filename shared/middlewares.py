"""
"""

from aiohttp import web
from .logging import get_logger


@web.middleware
async def response_middleware(request, handler):
    try:
        response = await handler(request)

    except web.HTTPUnauthorized:
        raise web.HTTPFound("/login")

    except web.HTTPFound:
        raise

    except Exception as exc:
        get_logger("root").exception(exc)
        raise web.HTTPFound("/login")

    else:
        return response
