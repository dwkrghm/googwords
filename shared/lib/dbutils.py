"""
shared utility functions for sql functions
"""

from sqlalchemy.sql import table


def qualified_sql_name(el):
    return f"{el.schema}.{el.name}" if el.schema else f"{el.name}"


def refresh_materialized_view(mview, *, concurrently: bool = True):
    _con = "CONCURRENTLY" if concurrently else ""
    return f"REFRESH MATERIALIZED VIEW {_con} {qualified_sql_name(mview)}"


def view(name, metadata, selectable):
    t = table(name)
    t.schema = metadata.schema

    for c in selectable.c:
        c._make_proxy(t)

    return t


def materialized_view(name, metadata, selectable):
    t = table(name)
    t.schema = metadata.schema

    for c in selectable.c:
        c._make_proxy(t)

    return t


class StoredProcedure:
    def __init__(self, name, sqltext):
        self.name = name
        self.sqltext = sqltext


class Trigger:
    def __init__(self, name, table, event, sqltext):
        self.name = name
        self.table = table
        self.event = event
        self.sqltext = sqltext
