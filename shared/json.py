"""
json serailizer with UUID and datetime support.

copied samelessly from django.core.serializers.json
"""

import datetime
import decimal
import json
import uuid
from functools import partial
from pathlib import PosixPath

# import ujson

__all__ = ["loads", "dumps"]


class JSONEncoder(json.JSONEncoder):
    """
    JSONEncoder subclass that knows how to encode date/time, decimal types, and
    UUIDs.
    """

    def default(self, o):  # pylint: disable=E0202
        # See "Date Time String Format" in the ECMA-262 specification.
        if isinstance(o, datetime.datetime):
            r = o.isoformat()
            # if o.microsecond:
            #     r = r[:23] + r[26:]
            # if r.endswith('+00:00'):
            #     r = r[:-6] + 'Z'
            return r
        elif isinstance(o, datetime.date):
            return o.isoformat()
        # elif isinstance(o, datetime.time):
        #     if is_aware(o):
        #         raise ValueError("JSON can't represent timezone-aware times.")
        #     r = o.isoformat()
        #     if o.microsecond:
        #         r = r[:12]
        #     return r
        # elif isinstance(o, datetime.timedelta):
        #     return duration_iso_string(o)
        elif isinstance(o, (decimal.Decimal, uuid.UUID, PosixPath)):
            return str(o)
        else:
            return super().default(o)


# loads = ujson.loads
loads = json.loads
dumps = partial(json.dumps, cls=JSONEncoder)
JSONDecodeError = json.decoder.JSONDecodeError
