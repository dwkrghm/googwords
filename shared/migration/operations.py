"""
"""

from alembic.operations import Operations, MigrateOperation


class ReversibleOp(MigrateOperation):
    def __init__(self, target):
        self.target = target

    @classmethod
    def invoke_for_target(cls, operations, target):
        op = cls(target)
        return operations.invoke(op)

    def reverse(self):
        raise NotImplementedError()

    @classmethod
    def _get_object_from_version(cls, operations, ident):
        version, objname = ident.split(".")

        module = operations.get_context().script.get_revision(version).module
        obj = getattr(module, objname)
        return obj

    @classmethod
    def replace(cls, operations, target, replaces=None, replace_with=None):
        if replaces:
            old_obj = cls._get_object_from_version(operations, replaces)
            drop_old = cls(old_obj).reverse()
            create_new = cls(target)
        elif replace_with:
            old_obj = cls._get_object_from_version(operations, replace_with)
            drop_old = cls(old_obj).reverse()
            create_new = cls(target)
        else:
            raise TypeError("replaces or replace_with is required")

        operations.invoke(drop_old)
        operations.invoke(create_new)


# stored procedures
@Operations.register_operation("create_storedprocedure", "invoke_for_target")
@Operations.register_operation("replace_storedprocedure", "replace")
class CreateStoredProcedureOp(ReversibleOp):
    def reverse(self):
        return DropStoredProcedureOp(self.target)


@Operations.register_operation("drop_storedprocedure", "invoke_for_target")
class DropStoredProcedureOp(ReversibleOp):
    def reverse(self):
        return CreateStoredProcedureOp(self.target)


@Operations.implementation_for(CreateStoredProcedureOp)
def create_storedprocedure(operations, operation):
    operations.execute(
        "CREATE FUNCTION %s %s" % (operation.target.name, operation.target.sqltext)
    )


@Operations.implementation_for(DropStoredProcedureOp)
def drop_storedprocedure(operations, operation):
    operations.execute("DROP FUNCTION IF EXISTS %s" % operation.target.name)


# triggers
@Operations.register_operation("create_trigger", "invoke_for_target")
@Operations.register_operation("replace_trigger", "replace")
class CreateTriggerOp(ReversibleOp):
    def reverse(self):
        return DropTriggerOp(self.target)


@Operations.register_operation("drop_trigger", "invoke_for_target")
class DropTriggerOp(ReversibleOp):
    def reverse(self):
        return CreateTriggerOp(self.target)


@Operations.implementation_for(CreateTriggerOp)
def create_trigger(operations, operation):
    operations.execute(
        "CREATE TRIGGER %s %s ON %s %s"
        % (
            operation.target.name,
            operation.target.event,
            operation.target.table,
            operation.target.sqltext,
        )
    )


@Operations.implementation_for(DropTriggerOp)
def drop_trigger(operations, operation):
    operations.execute(
        "DROP TRIGGER %s ON %s" % (operation.target.name, operation.target.table)
    )
