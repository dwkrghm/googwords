"""[summary]
"""

from sqlalchemy.ext import compiler
from sqlalchemy.schema import DDLElement

from shared.lib.dbutils import qualified_sql_name


class CreateView(DDLElement):
    def __init__(self, name, schema, selectable):
        self.name = name
        self.schema = schema
        self.selectable = selectable


class DropView(DDLElement):
    def __init__(self, name, schema):
        self.name = name
        self.schema = schema


@compiler.compiles(CreateView)
def _compile_create_view(element, compiler, **kwargs):
    return "CREATE OR REPLACE VIEW %s AS %s" % (
        qualified_sql_name(element),
        compiler.sql_compiler.process(element.selectable, literal_binds=True),
    )


@compiler.compiles(DropView)
def _compile_drop_view(element, compiler, **kwargs):
    return f"DROP VIEW IF EXISTS {qualified_sql_name(element)}"


class CreateMaterializedView(DDLElement):
    def __init__(self, name, schema, selectable):
        self.name = name
        self.schema = schema
        self.selectable = selectable


class DropMaterializedView(DDLElement):
    def __init__(self, name, schema):
        self.name = name
        self.schema = schema


@compiler.compiles(CreateMaterializedView)
def _compile_create_mview(element, compiler, **kwargs):
    return "CREATE MATERIALIZED VIEW %s AS %s" % (
        qualified_sql_name(element),
        compiler.sql_compiler.process(element.selectable, literal_binds=True),
    )


@compiler.compiles(DropMaterializedView)
def _compile_drop_mview(element, compiler, **kwargs):
    return f"DROP MATERIALIZED VIEW IF EXISTS {qualified_sql_name(element)}"


class CreateViewIndex(DDLElement):
    def __init__(self, index, view, column):
        self.name = index.name
        self.view = view
        self.column = column
        self.unique = index.unique


class DropViewIndex(DDLElement):
    def __init__(self, index):
        self.name = index.name


@compiler.compiles(CreateViewIndex)
def _compile_create_index(element, compiler, **kwargs):
    return "CREATE %s INDEX IF NOT EXISTS %s ON %s (%s)" % (
        "UNIQUE" if element.unique else "",
        element.name,
        qualified_sql_name(element.view),
        element.column,
    )


@compiler.compiles(DropViewIndex)
def _compile_drop_index(element, compiler, **kwargs):
    return f"DROP INDEX IF EXISTS {element.name}"
