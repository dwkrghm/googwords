"""
"""

from aiohttp import web
from aiopg.sa import create_engine
from aiohttp_session import setup as setup_session
from aiohttp_session.cookie_storage import EncryptedCookieStorage

from shared import logging


def init_logging(app: web.Application):
    logging.setup_logging(None, app["settings"].LOG_FILE)
    # logging.get_logger("aiopg").setLevel(logging.DEBUG)
    # logging.get_logger("sqlalchemy").setLevel(logging.DEBUG)


async def init_pgengine(app: web.Application):
    app["pg_engine"] = await create_engine(app["settings"].PG_DSN)
    yield
    app["pg_engine"].close()
    await app["pg_engine"].wait_closed()


def init_session(app: web.Application):
    setup_session(app, EncryptedCookieStorage(app["settings"].SECRET_KEY))
