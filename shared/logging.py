"""
"""

import logging
from logging.handlers import RotatingFileHandler


def _log_formatter():
    return logging.Formatter(
        fmt="[%(asctime)s] [%(name)s] [%(levelname)s] %(message)s",
        datefmt="%Y-%m-%d %H:%M:%S %z",
    )


get_logger = logging.getLogger


def _log_handler(handler, formatter):
    handler.setLevel(logging.DEBUG)
    handler.setFormatter(formatter)
    return handler


def _setup_logger(logger, handler):
    logger.setLevel(logging.DEBUG)
    logger.addHandler(_log_handler(handler, _log_formatter()))


def setup_logging(name, logfile):
    logger = logging.getLogger(name)
    logger.propagate = False
    _setup_logger(logger, logging.StreamHandler())
    if logfile:
        _setup_logger(logger, RotatingFileHandler(logfile))
