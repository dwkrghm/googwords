"""
"""

import os
from pathlib import Path

import yaml
from dotenv import load_dotenv
from pydantic import BaseSettings, PostgresDsn, ValidationError


THIS_DIR = Path(__file__).parent
BASE_PATH = THIS_DIR.parent
ENV_PREFIX = "AIO_APP_"
TEMPLATES_PATH = BASE_PATH / "app" / "templates"
STATIC_PATH = BASE_PATH / "static"

load_dotenv(dotenv_path=BASE_PATH / ".env")


def load_config():
    is_testing = os.getenv(ENV_PREFIX + "IS_TESTING", None)
    _env = None
    if is_testing == "1":
        _env = "testing"
    else:
        _env = os.getenv(ENV_PREFIX + "ENV", "production")
    config_file = BASE_PATH.joinpath(f"config/{_env}.yaml")
    if not config_file.exists():
        raise FileNotFoundError(f"Config file {config_file} doesnot exists.")

    return yaml.load(open(config_file, "r").read(), Loader=yaml.Loader)


class Settings(BaseSettings):
    BASE_PATH: Path
    TEMPLATES_PATH: Path
    STATIC_PATH: Path
    DEBUG: bool
    TESTING: bool
    UPLOAD_PATH: Path
    LOG_FILE: Path

    # db settings
    PG_DSN: PostgresDsn
    PG_POOL_MIN: int
    PG_POOL_MAX: int

    # google url
    SEARCH_URL: str = "https://www.google.com/search"

    # security
    SECRET_KEY: str

    class Config:
        env_prefix = ENV_PREFIX
        fields = {"PG_DSN": {"env": "AIO_APP_POSTGRES_DSN"}}

    def __init__(self, **kwargs):
        super().__init__(
            BASE_PATH=BASE_PATH,
            TEMPLATES_PATH=TEMPLATES_PATH,
            STATIC_PATH=STATIC_PATH,
            **kwargs,
        )


def settings_factory():
    try:
        settings = Settings(**load_config())
    except ValidationError:
        raise RuntimeError("Validation Failed.")
    else:
        return settings
