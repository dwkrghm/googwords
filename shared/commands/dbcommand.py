"""
"""

from alembic import command
from alembic.config import Config
from sqlalchemy import create_engine

from shared import json
from app.auth.models import create_user


def dbengine(dsn):
    return create_engine(dsn, json_serializer=json.dumps, json_deserializer=json.loads)


class DbCommand:
    def __init__(self, settings):
        # load alembic config
        alembic_ini_file = settings.BASE_PATH.joinpath("alembic.ini")
        self.alembic_cfg = Config(alembic_ini_file)
        self.alembic_cfg.set_main_option("sqlalchemy.url", settings.PG_DSN)
        self.alembic_cfg.set_main_option("script_location", "migrations")
        # create db engine
        self.__engine = create_engine(settings.PG_DSN)
        # load sub commands

    def current(self):
        """
        show current revision
        """
        command.current(self.alembic_cfg)

    def heads(self):
        """
        show available heads
        """
        command.heads(self.alembic_cfg)

    def history(self):
        """
        show history of revisions
        """
        command.history(self.alembic_cfg)

    def makemigrations(self, auto=True, message=None):
        """
        auto generate migrations
        """
        command.revision(self.alembic_cfg, autogenerate=auto, message=message)

    def migrate(self, revision="head", sql=False):
        """
        migrate up to specific revision, default revision is head
        tag is not supported yet
        """
        command.upgrade(self.alembic_cfg, revision=revision, sql=sql, tag=None)

    def rollback(self, revision):
        """
        rollback to specific version
        """
        command.downgrade(self.alembic_cfg, revision=revision)

    def createviews(self):
        """ create materialized views """

    def dropviews(self):
        """ drop materialized views """

    def resetviews(self):
        """ reset materialized views """
        self.dropviews()
        self.createviews()

    def refreshviews(self):
        """ refresh materialized views """

    def createsuperuser(self, login, password):
        self.__engine.execute(
            create_user(login=login, password=password, is_superuser=True)
        )
