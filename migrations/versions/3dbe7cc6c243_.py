"""empty message

Revision ID: 3dbe7cc6c243
Revises: 590b241bd14e
Create Date: 2020-01-25 12:01:24.867219

"""
from alembic import op
import sqlalchemy as sa  # noqa: F401


# revision identifiers, used by Alembic.
revision = "3dbe7cc6c243"
down_revision = "590b241bd14e"
branch_labels = None
depends_on = None


def upgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.create_index(
        op.f("ix_search_results_keyword"), "search_results", ["keyword"], unique=False
    )
    op.create_index(
        "idx_user_uploads_keywords",
        "user_uploads",
        ["keywords"],
        unique=False,
        postgresql_using="gin",
    )
    # ### end Alembic commands ###


def downgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.drop_index("idx_user_uploads_keywords", table_name="user_uploads")
    op.drop_index(op.f("ix_search_results_keyword"), table_name="search_results")
    # ### end Alembic commands ###
