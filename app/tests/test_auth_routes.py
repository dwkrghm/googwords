"""
test auth api routes
"""


async def test_login_with_get(cli):
    resp = await cli.get("/login")
    assert resp.status == 405


async def test_login_with_empty_post(cli):
    resp = await cli.post("/login", data={})
    # print(await resp.json())
    assert resp.status == 400


async def test_login_with_valid_user(cli, with_fake_user):
    resp = await cli.post("/login", data={"username": "testuser", "password": "dummy"})
    assert resp.status == 200
    assert await resp.json()


async def test_login_with_invalid_user(cli, with_fake_user):
    resp = await cli.post("/login", data={"username": "test", "password": "dummy"})
    assert resp.status == 401
    assert await resp.json()


async def test_login_with_invalid_password(cli, with_fake_user):
    resp = await cli.post("/login", data={"username": "testuser", "password": "dumdum"})
    assert resp.status == 401
    assert await resp.json()


async def test_logout_without_login(cli):
    resp = await cli.get("/logout")
    assert resp.status == 401


async def test_logout_with_login_session(with_loggedin_user):
    # then try to logout
    resp = await with_loggedin_user.get("/logout")
    assert resp.status == 200
    assert await resp.json()
