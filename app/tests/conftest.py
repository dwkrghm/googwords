"""
fixtures for testing routes
"""


import pytest


@pytest.fixture
async def with_loggedin_user(cli, with_fake_user):
    # login user first
    resp = await cli.post("/login", data={"username": "testuser", "password": "dummy"})
    assert resp.status == 200
    yield cli
