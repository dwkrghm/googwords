"""
"""


async def test_upload_route(cli):
    resp = await cli.post("/upload")
    assert resp.status == 401


async def test_upload_without_data(with_loggedin_user):
    resp = await with_loggedin_user.post("/upload")
    assert resp.status == 400


async def test_upload_with_empty_file(tmp_path, with_loggedin_user):
    _file = tmp_path / "hello.txt"
    _file.write_text("")
    files = {"file": open(_file, "rb")}
    resp = await with_loggedin_user.post("/upload", data=files)
    assert resp.status == 400


async def test_upload_with_invalid_filetype(tmp_path, with_loggedin_user):
    _file = tmp_path / "hello.gif"
    _file.write_text("GIF87a�����,�H;\n")
    files = {"file": open(_file, "rb")}
    resp = await with_loggedin_user.post("/upload", data=files)
    assert resp.status == 400


async def test_upload_with_text(tmp_path, with_loggedin_user):
    _file = tmp_path / "hello.text"
    _file.write_text("hello")
    files = {"file": open(_file, "rb")}
    resp = await with_loggedin_user.post("/upload", data=files)
    assert resp.status == 200
