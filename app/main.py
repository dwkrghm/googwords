"""

TODO: create cli to generate secret key
```
from cryptography.fernet import Fernet
Fernet.generate_key()
```
"""

from aiohttp import web
from aiohttp_jinja2 import setup as setup_jinja2
from aiohttp_security import setup as setup_security
from aiohttp_security import SessionIdentityPolicy
from aiojobs.aiohttp import setup as setup_aiojobs
from jinja2 import FileSystemLoader

from shared.bootstrap import init_pgengine, init_logging, init_session
from shared.middlewares import response_middleware
from shared.settings import settings_factory

from .auth.policies import DBAuthorizationPolicy
from .routes import register_routes


async def init_security(app: web.Application):
    setup_security(
        app, SessionIdentityPolicy(), DBAuthorizationPolicy(app["pg_engine"])
    )
    yield


def create_app() -> web.Application:
    app = web.Application(middlewares=[response_middleware])
    app.update(name="main", settings=settings_factory())
    # setup logging is first thing anyone wants to do
    init_logging(app)
    init_session(app)
    setup_aiojobs(app)
    setup_jinja2(app, loader=FileSystemLoader(str(app["settings"].TEMPLATES_PATH)))
    # cleanup contexts
    app.cleanup_ctx.append(init_pgengine)
    app.cleanup_ctx.append(init_security)

    # register application routes
    register_routes(app)
    return app
