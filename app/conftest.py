"""
pytest fixtures and setups
"""

import os
from datetime import datetime
from pathlib import Path

import pytest
import sqlalchemy as sa
from aiohttp import web
from aiopg.sa import create_engine
from dotenv import load_dotenv
from passlib.hash import pbkdf2_sha256


BASE_PATH = Path(__file__).parent.parent
load_dotenv(dotenv_path=BASE_PATH / ".env")


@pytest.fixture
async def db_engine(loop):
    """ init db engine for testing """
    # setup
    yield await create_engine(os.getenv("AIO_APP_PG_TEST_DSN"))


@pytest.fixture
async def auth_policy(db_engine):
    from app.auth.policies import DBAuthorizationPolicy

    yield DBAuthorizationPolicy(db_engine)


@pytest.fixture
async def user_model_fixture(db_engine):
    from app.auth import dbtables

    async with db_engine.acquire() as conn:
        # setup
        await conn.execute(sa.schema.CreateTable(dbtables.users))
        yield
        # tear down
        await conn.execute(sa.schema.DropTable(dbtables.users))


@pytest.fixture
async def search_result_model_fixture(db_engine):
    from app.crawler import dbtables

    async with db_engine.acquire() as conn:
        # setup
        await conn.execute(sa.schema.CreateTable(dbtables.search_results))
        yield
        # tear down
        await conn.execute(sa.schema.DropTable(dbtables.search_results))


@pytest.fixture
async def with_fake_user(user_model_fixture, db_engine):
    async with db_engine.acquire() as conn:
        result = await conn.execute(
            """INSERT INTO users(login, password, is_superuser, disabled, created_on, modified_on)
            VALUES(%s, %s, %s, %s, %s, %s) RETURNING id""",
            (
                "testuser",
                pbkdf2_sha256.hash("dummy"),
                False,
                False,
                datetime.now(),
                datetime.now(),
            ),
        )
        yield await result.fetchone()
        await conn.execute("""DELETE FROM users WHERE login=%s""", ("testuser",))


@pytest.fixture
async def with_fake_disabled_user(user_model_fixture, db_engine):
    async with db_engine.acquire() as conn:
        result = await conn.execute(
            """INSERT INTO users(login, password, is_superuser, disabled, created_on, modified_on)
            VALUES(%s, %s, %s, %s, %s, %s) RETURNING id""",
            (
                "testuser",
                pbkdf2_sha256.hash("dummy"),
                False,
                True,
                datetime.now(),
                datetime.now(),
            ),
        )
        yield await result.fetchone()
        await conn.execute("""DELETE FROM users WHERE login=%s""", ("testuser",))


@pytest.fixture
async def with_fake_superuser(user_model_fixture, db_engine):
    async with db_engine.acquire() as conn:
        result = await conn.execute(
            """INSERT INTO users(login, password, is_superuser, disabled, created_on, modified_on)
            VALUES(%s, %s, %s, %s, %s, %s) RETURNING id""",
            (
                "testuser",
                pbkdf2_sha256.hash("dummy"),
                True,
                False,
                datetime.now(),
                datetime.now(),
            ),
        )
        yield await result.fetchone()
        await conn.execute("""DELETE FROM users WHERE login=%s""", ("testuser",))


@pytest.fixture
def cli(loop, aiohttp_client):
    """ test app instance """
    from aiohttp_security import setup as setup_security
    from aiohttp_security import SessionIdentityPolicy
    from aiohttp_session import setup as setup_session
    from aiohttp_session.cookie_storage import EncryptedCookieStorage
    from app.auth import views as auth_views
    from app.auth.policies import DBAuthorizationPolicy
    from app.crawler import views as cviews

    async def init_pgengine(app):
        app["pg_engine"] = await create_engine(os.getenv("AIO_APP_PG_TEST_DSN"))
        yield
        app["pg_engine"].close()
        await app["pg_engine"].wait_closed()

    async def init_security(app: web.Application):
        setup_security(
            app, SessionIdentityPolicy(), DBAuthorizationPolicy(app["pg_engine"])
        )
        yield

    # init application
    app = web.Application()
    setup_session(app, EncryptedCookieStorage(os.getenv("AIO_APP_SECRET_KEY")))
    app.cleanup_ctx.append(init_pgengine)
    app.cleanup_ctx.append(init_security)

    # load routes
    app.router.add_post("/login", auth_views.login)
    app.router.add_get("/logout", auth_views.logout)
    app.router.add_post("/upload", cviews.upload)

    return loop.run_until_complete(aiohttp_client(app))
