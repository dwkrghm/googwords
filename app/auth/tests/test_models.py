"""
"""

from app.auth import models


async def test_select_with_existing_user(with_fake_user, db_engine):
    # print(with_fake_user)
    async with db_engine.acquire() as conn:
        result = await conn.execute(models.select_user_by_login("testuser"))
        assert await result.fetchone() is not None


async def test_select_with_nonexisting_user(with_fake_user, db_engine):
    async with db_engine.acquire() as conn:
        result = await conn.execute(models.select_user_by_login("notauser"))
        assert await result.fetchone() is None


async def test_user_exists_with_existing_user(with_fake_user, db_engine):
    async with db_engine.acquire() as conn:
        result = await conn.scalar(models.login_exists("testuser"))
        assert result


async def test_user_exists_with_nonexisting_user(with_fake_user, db_engine):
    async with db_engine.acquire() as conn:
        result = await conn.scalar(models.login_exists("notauser"))
        assert not result
