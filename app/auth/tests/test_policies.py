"""
test user policies
"""

from app.auth.policies import check_credentials


class TestAuthorizedUser:
    async def test_normal_user_should_pass(self, with_fake_user, auth_policy):
        assert await auth_policy.authorized_userid("testuser") == "testuser"

    async def test_disabled_user_should_fail(
        self, with_fake_disabled_user, auth_policy
    ):
        assert await auth_policy.authorized_userid("testuser") is None

    async def test_nonexisting_user_should_fail(self, with_fake_user, auth_policy):
        assert await auth_policy.authorized_userid("notauser") is None


class TestPermitsUser:
    async def test_normal_user_should_fail(self, with_fake_user, auth_policy):
        assert not await auth_policy.permits("testuser")

    async def test_superuser_should_pass(self, with_fake_superuser, auth_policy):
        assert await auth_policy.permits("testuser")


class TestCredentials:
    async def test_good_password(self, with_fake_user, db_engine):
        assert await check_credentials(db_engine, "testuser", "dummy")

    async def test_bad_password(self, with_fake_user, db_engine):
        assert not await check_credentials(db_engine, "testuser", "password")
