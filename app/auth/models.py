"""
"""

import sqlalchemy as sa

from pydantic import BaseModel
from passlib.hash import pbkdf2_sha256

from .dbtables import users


class LoginModel(BaseModel):
    username: str
    password: str


def select_user_by_login(login):
    return sa.select([users]).where(
        sa.and_(users.c.login == login, sa.not_(users.c.disabled))
    )


def login_exists(login):
    return sa.select(
        [
            sa.exists().where(
                sa.and_(
                    sa.func.lower(users.c.login) == login, sa.not_(users.c.disabled)
                )
            )
        ]
    )


# def select_permissions_by_user_id(user_id):
#     return sa.select([permissions]).where(permissions.c.user_id == user_id)


def create_user(*, login, password, is_superuser=False, disabled=False):
    return (
        sa.insert(users)
        .returning(users.c.id)
        .values(
            login=login.strip(),
            password=pbkdf2_sha256.hash(password.strip()),
            is_superuser=is_superuser,
            disabled=disabled,
        )
    )
