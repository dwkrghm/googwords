"""
"""

from aiohttp_security.abc import AbstractAuthorizationPolicy
from passlib.hash import pbkdf2_sha256

from . import models


class DBAuthorizationPolicy(AbstractAuthorizationPolicy):
    def __init__(self, db_engine):
        self.db_engine = db_engine

    async def authorized_userid(self, identity):
        async with self.db_engine.acquire() as conn:
            result = await conn.execute(models.select_user_by_login(identity))
            user = await result.fetchone()
            if user is not None:
                return user[0]
            return None

    async def permits(self, identity, permission=None, context=None):
        if identity is None:
            return False

        async with self.db_engine.acquire() as conn:
            result = await conn.execute(models.select_user_by_login(identity))
            user = await result.fetchone()
            if user is not None:
                is_superuser = user[3]
                if is_superuser:
                    return True
        return False


async def check_credentials(db_engine, username, password):
    async with db_engine.acquire() as conn:
        result = await conn.execute(models.select_user_by_login(username))
        user = await result.fetchone()
        if user is not None:
            user_password_hash = user[2]
            return pbkdf2_sha256.verify(password, user_password_hash)
    return False
