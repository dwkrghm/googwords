"""
"""

import aiohttp_jinja2
from aiohttp import web
from aiohttp_security import check_authorized, remember, forget
from pydantic import ValidationError

from .models import LoginModel
from .policies import check_credentials


@aiohttp_jinja2.template("auth/login.jinja2.html")
async def login(request: web.Request):
    # session = await aiohttp_session.get_session(request)
    if request.method == "POST":
        form = await request.post()
        try:
            model = LoginModel(**form)
        except ValidationError:
            return {"error": "Validation Failed"}
        else:
            # validate db here
            valid_user = await check_credentials(
                request.app["pg_engine"], model.username, model.password
            )
            if valid_user:
                await remember(request, web.HTTPFound("/"), model.username)
                raise web.HTTPFound("/")
            return {"error": "Invalid Username or Password", "username": model.username}
    return {}


async def logout(request: web.Request):
    await check_authorized(request)
    return_redirect = web.HTTPFound("/")
    await forget(request, return_redirect)
    raise return_redirect
