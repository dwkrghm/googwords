"""
"""

import sqlalchemy as sa

# from sqlalchemy.schema import ForeignKey

from shared.db import METADATA


users = sa.Table(
    "users",
    METADATA,
    sa.Column("id", sa.Integer, primary_key=True),
    sa.Column("login", sa.String(256), nullable=False, index=True),
    sa.Column("password", sa.String(256), nullable=False),
    sa.Column("is_superuser", sa.Boolean(), nullable=False, default=False),
    sa.Column("disabled", sa.Boolean(), nullable=False, default=False),
    sa.Column(
        "created_on",
        sa.TIMESTAMP(timezone=True),
        nullable=False,
        default=sa.func.now(),
    ),
    sa.Column(
        "modified_on",
        sa.TIMESTAMP(timezone=True),
        nullable=False,
        default=sa.func.now(),
        onupdate=sa.func.now(),
    ),
)

# permissions = sa.Table(
#     "permissions",
#     METADATA,
#     sa.Column("id", sa.Integer, primary_key=True),
#     sa.Column("user_id", sa.Integer, ForeignKey("users.id")),
#     sa.Column("perm_name", sa.String(64), nullable=False),
#     sa.Column(
#         "created_on",
#         sa.TIMESTAMP(timezone=True),
#         nullable=False,
#         default=sa.func.now(),
#     ),
#     sa.Column(
#         "modified_on",
#         sa.TIMESTAMP(timezone=True),
#         nullable=False,
#         default=sa.func.now(),
#         onupdate=sa.func.now(),
#     ),
# )
