"""
"""

import aiohttp
import pytest


@pytest.fixture
async def client_session(loop):
    headers = {"User-Agent": "Mozilla/4.0 (compatible; MSIE 8.0; Windows NT 6.0)"}
    async with aiohttp.ClientSession(headers=headers) as session:
        yield session
