"""
"""

from app.crawler import tasks


async def test_google_search(db_engine, search_result_model_fixture):
    await tasks.search_and_extract(
        db_engine, ["holiday", "bangkok", "kathmandu", "london"]
    )
