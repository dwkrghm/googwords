"""
crawler jobs
"""

import re
from selenium.webdriver import Chrome
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.common.keys import Keys

from .models import create_search_results, update_user_uploads_status


async def search_and_extract(db_engine, keywords, upload_id):
    """
    """
    url = "https://www.google.com?hl=en"
    options = Options()
    options.add_argument("--headless")
    with Chrome(options=options) as driver:
        driver.get(url)
        for keyword in keywords:
            driver.implicitly_wait(10)
            search_box = driver.find_element_by_name("q")
            search_box.send_keys(keyword + Keys.ENTER)

            # cleanup data
            stats = driver.find_element_by_id("resultStats")
            num_links = len(driver.find_elements_by_tag_name("a"))
            num_adwords = len(driver.find_elements_by_class_name("ads-ad"))
            # adword didn't load on browser :TODO: add lateron
            # num_local_ads = driver.find_elements_by_class_name("ccBEnf")
            # class = "ads-ad"
            if stats:
                matches = re.match(r"About ([0-9,]+) results", stats.text)
                num_results = int(re.sub(",", "", matches.group(1)))
                async with db_engine.acquire() as conn:
                    await conn.execute(
                        create_search_results(
                            user_upload_id=upload_id,
                            keyword=keyword,
                            num_adwords=num_adwords,
                            num_links=num_links,
                            num_results=num_results,
                            page_cache=driver.page_source,
                            status="completed",
                        )
                    )
            driver.back()
    async with db_engine.acquire() as conn:
        await conn.execute(update_user_uploads_status(upload_id, status="completed"))
