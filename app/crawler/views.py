"""
"""

import re
import magic
import aiohttp_jinja2
import aiohttp_session
from aiohttp import web
from aiohttp_security import check_authorized, authorized_userid
from aiojobs.aiohttp import spawn

from .tasks import search_and_extract
from .models import (
    create_user_uploads,
    select_user_uploads,
    select_search_results_by_user_keywords,
    select_latest_user_uploads,
    select_search_results_by_upload,
    select_search_results_by_keyword,
)


@aiohttp_jinja2.template("crawler/dashboard.jinja2.html")
async def home(request):
    await check_authorized(request)
    loggedin_user = await authorized_userid(request)
    session = await aiohttp_session.get_session(request)
    async with request.app["pg_engine"].acquire() as conn:
        result = await conn.execute(select_latest_user_uploads(loggedin_user))
        records = await result.fetchall()
    return {"session": session, "records": records}


@aiohttp_jinja2.template("crawler/report.jinja2.html")
async def report(request):
    await check_authorized(request)
    loggedin_user = await authorized_userid(request)
    session = await aiohttp_session.get_session(request)
    async with request.app["pg_engine"].acquire() as conn:
        result = await conn.execute(
            select_search_results_by_user_keywords(loggedin_user)
        )
        records = await result.fetchall()
    return {"session": session, "records": records}


async def upload(request: web.Request):
    await check_authorized(request)
    loggedin_user = await authorized_userid(request)
    session = await aiohttp_session.get_session(request)
    post = await request.post()
    uploaded_file = post.get("file")
    if uploaded_file:
        content_length = int(uploaded_file.headers.get("Content-Length", 0))
        if 10_00_000 < content_length < 1:
            session["error"] = "File is either too large or empty"
            raise web.HTTPFound("/")
        content = uploaded_file.file.read()
        if magic.from_buffer(content, mime=True) != "text/plain":
            session["error"] = "Unsupported file type"
            raise web.HTTPFound("/")
        words = re.sub("\r\n", ",", content.decode())
        keywords = words.strip("\n").split(",")
        # TODO: update later
        lastinsertid = None
        async with request.app["pg_engine"].acquire() as conn:
            result = await conn.execute(
                create_user_uploads(
                    user_id=loggedin_user, keywords=keywords, status="pending"
                )
            )
            lastinsertid = await result.fetchone()
        await spawn(
            request,
            search_and_extract(request.app["pg_engine"], keywords, lastinsertid.id),
        )
        raise web.HTTPFound("/")
    raise web.HTTPFound("/")


@aiohttp_jinja2.template("crawler/uploads.jinja2.html")
async def report_all_uploads(request):
    await check_authorized(request)
    loggedin_user = await authorized_userid(request)
    session = await aiohttp_session.get_session(request)
    async with request.app["pg_engine"].acquire() as conn:
        result = await conn.execute(select_user_uploads(loggedin_user))
        records = await result.fetchall()
    return {"session": session, "records": records}


@aiohttp_jinja2.template("crawler/keywords.jinja2.html")
async def report_by_upload(request):
    await check_authorized(request)
    upload_id = request.match_info["id"]
    try:
        upload_id = int(upload_id)
    except ValueError:
        raise web.HTTPFound("/")
    # loggedin_user = await authorized_userid(request)
    session = await aiohttp_session.get_session(request)
    async with request.app["pg_engine"].acquire() as conn:
        result = await conn.execute(select_search_results_by_upload(upload_id))
        records = await result.fetchall()
    return {"session": session, "records": records}


@aiohttp_jinja2.template("crawler/keywords.jinja2.html")
async def report_all_keywords(request):
    await check_authorized(request)
    loggedin_user = await authorized_userid(request)
    session = await aiohttp_session.get_session(request)
    async with request.app["pg_engine"].acquire() as conn:
        result = await conn.execute(
            select_search_results_by_user_keywords(loggedin_user)
        )
        records = await result.fetchall()
    return {"session": session, "records": records}


@aiohttp_jinja2.template("crawler/keywords.jinja2.html")
async def report_by_keyword(request):
    await check_authorized(request)
    # loggedin_user = await authorized_userid(request)
    session = await aiohttp_session.get_session(request)
    keyword = request.match_info["keyword"]
    async with request.app["pg_engine"].acquire() as conn:
        result = await conn.execute(select_search_results_by_keyword(keyword))
        records = await result.fetchall()
    return {"session": session, "records": records}
