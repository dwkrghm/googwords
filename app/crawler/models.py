"""
"""

import sqlalchemy as sa
from .dbtables import user_uploads, search_results


def select_latest_user_uploads(user_id: int):
    return (
        sa.select([user_uploads])
        .where(user_uploads.c.user_id == user_id)
        .order_by(user_uploads.c.created_on.desc())
        .limit(10)
    )


def select_user_uploads(user_id: int):
    return (
        sa.select([user_uploads])
        .where(user_uploads.c.user_id == user_id)
        .order_by(user_uploads.c.created_on.desc())
    )


def create_user_uploads(*, user_id, keywords, status, filename=""):
    return (
        sa.insert(user_uploads)
        .returning(user_uploads.c.id)
        .values(user_id=user_id, filename=filename, keywords=keywords, status=status,)
    )


def update_user_uploads_status(pkey, *, status):
    return (
        sa.update(user_uploads)
        .returning(user_uploads.c.id)
        .where(user_uploads.c.id == pkey)
        .values(status=status)
    )


def select_search_results_by_keywords(subquery):
    # do group by keyword and select latest with status "ok"
    subq = (
        sa.select(
            [
                search_results.c.id,
                sa.func.max(search_results.c.modified_on).label("maxdate"),
            ]
        )
        .where(
            sa.and_(
                search_results.c.keyword.in_(subquery),
                search_results.c.status == "completed",
            )
        )
        .group_by(search_results.c.id)
    ).alias()

    joined = search_results.join(
        subq,
        sa.and_(
            search_results.c.id == subq.c.id,
            search_results.c.modified_on == subq.c.maxdate,
        ),
    )
    return (
        sa.select([search_results])
        .select_from(joined)
        .order_by(search_results.c.keyword)
    )


def select_search_results():
    return sa.select([search_results]).where(search_results.c.status == "completed")


def select_search_results_by_user_keywords(user_id):
    subq_k = (
        sa.select([sa.func.unnest(user_uploads.c.keywords).label("keywords")])
        .where(user_uploads.c.user_id == user_id)
        .alias()
    )
    # user_keywords = sa.select(
    #     [sa.func.array_agg(sa.func.distinct(subq_k.c.keywords)).label("keywords")]
    # ).select_from(subq_k).alias("q2")
    return select_search_results_by_keywords(
        sa.select([sa.func.distinct(subq_k.c.keywords).label("keywords")])
        .select_from(subq_k)
        .alias()
    )


def select_search_results_by_keyword(keyword: str):
    return (
        sa.select([search_results])
        .where(search_results.c.keyword.like(keyword))
        .order_by(search_results.c.modified_on.desc())
    )


def select_search_results_by_upload(upload_id):
    return (
        sa.select([search_results])
        .where(search_results.c.user_upload_id == upload_id)
        .order_by(search_results.c.keyword)
    )


def create_search_results(
    *, user_upload_id, keyword, num_adwords, num_links, num_results, page_cache, status
):
    return (
        sa.insert(search_results)
        .returning(search_results.c.id)
        .values(
            user_upload_id=user_upload_id,
            keyword=keyword,
            num_adwords=num_adwords,
            num_links=num_links,
            num_results=num_results,
            page_cache=page_cache,
            status=status,
        )
    )
