"""
"""

import sqlalchemy as sa
from sqlalchemy.schema import ForeignKey
from sqlalchemy.dialects.postgresql import ARRAY, ENUM
from sqlalchemy.schema import Index

from shared.db import METADATA


ENUM_STATUS = ENUM("completed", "pending", name="enum_status", metadata=METADATA)

user_uploads = sa.Table(
    "user_uploads",
    METADATA,
    sa.Column("id", sa.Integer, primary_key=True),
    sa.Column("user_id", sa.Integer, ForeignKey("users.id")),
    sa.Column("filename", sa.String(256), nullable=False),
    sa.Column("keywords", ARRAY(sa.String), nullable=False),
    sa.Column("status", ENUM_STATUS, nullable=False, default="pending"),
    sa.Column(
        "created_on",
        sa.TIMESTAMP(timezone=True),
        nullable=False,
        default=sa.func.now(),
    ),
    sa.Column(
        "modified_on",
        sa.TIMESTAMP(timezone=True),
        nullable=False,
        default=sa.func.now(),
        onupdate=sa.func.now(),
    ),
)
Index("idx_user_uploads_keywords", user_uploads.c.keywords, postgresql_using="gin")

search_results = sa.Table(
    "search_results",
    METADATA,
    sa.Column("id", sa.Integer, primary_key=True),
    sa.Column(
        "user_upload_id",
        sa.Integer,
        ForeignKey("user_uploads.id", name="search_results_user_upload_id_fkey"),
    ),
    sa.Column("keyword", sa.String(256), nullable=False, index=True),
    sa.Column("num_adwords", sa.Integer, nullable=False),
    sa.Column("num_links", sa.Integer, nullable=False),
    sa.Column("num_results", sa.BigInteger, nullable=False),
    sa.Column("page_cache", sa.Text, nullable=False),
    sa.Column("status", ENUM_STATUS, nullable=False, default="pending"),
    sa.Column(
        "created_on",
        sa.TIMESTAMP(timezone=True),
        nullable=False,
        default=sa.func.now(),
    ),
    sa.Column(
        "modified_on",
        sa.TIMESTAMP(timezone=True),
        nullable=False,
        default=sa.func.now(),
        onupdate=sa.func.now(),
    ),
)
