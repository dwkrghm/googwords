"""
application routes
"""

from .auth import views as aviews
from .crawler import views as cviews


def register_routes(app):
    login = app.router.add_resource("/login", name="login")
    login.add_route("GET", aviews.login)
    login.add_route("POST", aviews.login)
    logout = app.router.add_resource("/logout", name="logout")
    logout.add_route("GET", aviews.logout)
    upload = app.router.add_resource("/upload", name="upload")
    upload.add_route("POST", cviews.upload)
    by_upload = app.router.add_resource("/report/uploads/{id}", name="report_by_upload")
    by_upload.add_route("GET", cviews.report_by_upload)
    all_uploads = app.router.add_resource("/report/uploads", name="report_all_uploads")
    all_uploads.add_route("GET", cviews.report_all_uploads)
    by_keyword = app.router.add_resource(
        "/report/keywords/{keyword}", name="report_by_keyword"
    )
    by_keyword.add_route("GET", cviews.report_by_keyword)
    all_keywords = app.router.add_resource(
        "/report/keywords", name="report_all_keywords"
    )
    all_keywords.add_route("GET", cviews.report_all_keywords)
    home = app.router.add_resource("/", name="home")
    home.add_route("GET", cviews.home)
