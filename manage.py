"""
application management command
"""

import os
import asyncio
import fire
import uvloop
from aiohttp import web
from colorama import init, Style
from watchgod import run_process

from app.main import create_app
from shared.commands.dbcommand import DbCommand


class Management:
    def __init__(self, host="127.0.0.1", port=8008, monitor=False, test=False):
        if test:
            os.environ["AIO_APP_IS_TESTING"] = "1"
        self.host = host
        self.port = 7890 if test else port
        self.monitor = monitor
        print(Style.DIM)
        self.app = create_app()
        print(Style.RESET_ALL)
        self.db = DbCommand(self.app["settings"])

    def _run_app(self):
        """
        run aiohttp web runserver with uvloop

        # runner = web.AppRunner(setup_app(get_application_settings()))
        # await runner.setup()
        # site = web.TCPSite(runner, host=host, port=port)
        # await site.start()
        """
        asyncio.set_event_loop_policy(uvloop.EventLoopPolicy())
        loop = asyncio.get_event_loop()

        if self.app["settings"].DEBUG:
            loop.set_debug(True)

        web.run_app(self.app, host=self.host, port=self.port)

    def run(self):
        """
        """
        self._run_app()

    def runserver(self):
        """
        run application server in development mode
        """
        run_process(self.app["settings"].BASE_PATH, self._run_app)

    def generate_secret(self):
        """
        """
        from cryptography.fernet import Fernet

        print("Copy the string within quotes: {}".format(Fernet.generate_key()))


if __name__ == "__main__":
    init()
    fire.Fire(Management)
    print(Style.RESET_ALL)
